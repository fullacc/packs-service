package errors

import "errors"

var (
	BadRequest    = errors.New("error.bad_request")
	Forbidden     = errors.New("error.forbidden")
	InternalError = errors.New("error.internal_error")
	NotFound      = errors.New("error.not_found")
)
