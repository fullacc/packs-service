# Start from a small, secure base image
FROM golang:1.20-alpine AS builder

# Set the working directory inside the container
WORKDIR /build

# Copy the Go module files
COPY go.mod go.sum ./

# Download the Go module dependencies
RUN go mod download

# Copy the source code into the container
COPY . .

# Build the Go binary
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./bin/packs-service ./cmd/packs-service/main.go

# Create a minimal production image
FROM alpine:3.12

# It's essential to regularly update the packages within the image to include security patches
RUN apk update && apk upgrade

# Reduce image size
RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/*

# Avoid running code as a root user
RUN adduser -D appuser
USER appuser

# Set the working directory inside the container
WORKDIR /app

# Copy only the necessary files from the builder stage
COPY --from=builder /build/bin/* ./

# Set any environment variables required by the application
ENV HTTP_PORT=8100

# Expose the port that the application listens on
EXPOSE 8100

# Run the binary when the container starts
CMD ["./packs-service"]
