#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

-include include.make

APPS := packs-service

.PHONY:

help: Makefile
	@echo ""
	@echo "                      __                                   _          "
	@echo "    ____  ____ ______/ /_______      ________  ______   __(_)_______  "
	@echo "   / __ \/ __ `/ ___/ //_/ ___/_____/ ___/ _ \/ ___/ | / / / ___/ _ \ "
	@echo "  / /_/ / /_/ / /__/ ,< (__  )_____(__  )  __/ /   | |/ / / /__/  __/ "
	@echo " / .___/\__,_/\___/_/|_/____/     /____/\___/_/    |___/_/\___/\___/  "
	@echo "/_/                                                                   "
	@echo ""
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

$(APPS):
	@echo "Building $@..."
	@go build -o bin/$@ ./cmd/$@/

build: $(APPS) ## Build sources

codegen-oapi:
	@go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@d516da7
	oapi-codegen --config oapi-codegen.yaml docs/openapi.yaml

run-tests:
	go test ./...

generate-mocks:
	@go install github.com/vektra/mockery/v2@v2.31.4
	@go generate ./...

generate: generate-mocks codegen-oapi

dc:
	docker compose up -d --remove-orphans --build