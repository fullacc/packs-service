package main

import (
	"fmt"
	"gitlab.com/fullacc/packs-service/internal/config"
	"gitlab.com/fullacc/packs-service/internal/repository/inmem"
	"gitlab.com/fullacc/packs-service/internal/server/http"
	"gitlab.com/fullacc/packs-service/internal/service/handler"
	"gitlab.com/fullacc/packs-service/internal/service/pack"
	"log"
	"os"
	"os/signal"
	"syscall"
)

var ServiceName = "packs-service"

func main() {
	var err error

	conf, err := config.NewConfig()
	if err != nil {
		log.Panic("config", err)
	}
	if conf.ServiceName == "" {
		conf.ServiceName = ServiceName
	}
	packRepo := inmem.NewRepository()

	packService := pack.NewService(packRepo)

	handlerService := handler.NewService(
		packRepo,
	)

	server := http.NewServer(
		conf,
		handlerService,
		packService,
	)
	go func() {
		log.Println("starting http server")
		err = server.Start(fmt.Sprintf(":%s", conf.HTTPPort))
		if err != nil {
			log.Panic(fmt.Sprintf("server error: %v", err))
		}
	}()
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-stop
	_ = server.Close()
	os.Exit(0)
}
