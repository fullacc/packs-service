package config

import "os"

type Config struct {
	HTTPPort    string
	ServiceName string
}

func NewConfig() (*Config, error) {
	var conf Config
	httpPort, exists := os.LookupEnv("HTTP_PORT")
	if exists {
		conf.HTTPPort = httpPort
	} else {
		conf.HTTPPort = "8080"
	}
	return &conf, nil
}
