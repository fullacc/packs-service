package inmem

import (
	"context"
	"fmt"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"gitlab.com/fullacc/packs-service/internal/interfaces/repositories"
	"gitlab.com/fullacc/packs-service/pkg/errors"
	"sync"
	"time"
)

type repository struct {
	data map[int]*Pack
	mu   sync.RWMutex
}

func NewRepository() repositories.PackRepository {
	return &repository{
		data: make(map[int]*Pack),
	}
}

func (s *repository) List(_ context.Context) ([]domain.Pack, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()

	var packs []domain.Pack
	for _, v := range s.data {
		domainPack, err := packStoreToDomain(v)
		if err != nil {
			return nil, fmt.Errorf("packStoreToDomain failed: %w", err)
		}
		packs = append(packs, *domainPack)
	}

	return packs, nil
}

func (s *repository) Count(_ context.Context) (int, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()

	return len(s.data), nil
}

func (s *repository) Create(_ context.Context, p *domain.Pack) error {
	if p == nil {
		return errors.BadRequest
	}

	storePack := packDomainToStore(p)

	s.mu.Lock()
	defer s.mu.Unlock()

	_, exists := s.data[storePack.Size]
	if exists {
		return errors.BadRequest
	} else {
		storePack.CreatedAt = time.Now()

		s.data[storePack.Size] = storePack
		return nil
	}
}

func (s *repository) Delete(_ context.Context, size int) error {
	if size == 0 {
		return errors.BadRequest
	}

	s.mu.Lock()
	defer s.mu.Unlock()

	_, exists := s.data[size]
	if exists {
		delete(s.data, size)
		return nil
	} else {
		return errors.BadRequest
	}
}
