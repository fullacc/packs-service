package inmem

import (
	"context"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"gitlab.com/fullacc/packs-service/pkg/errors"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPortStore_CreatePack(t *testing.T) {
	t.Parallel()
	repo := NewRepository()

	t.Run("create pack", func(t *testing.T) {
		t.Parallel()

		randomPack := newRandomDomainPack(t)

		err := repo.Create(context.Background(), randomPack)
		require.NoError(t, err)

		packs, err := repo.List(context.Background())
		require.NoError(t, err)
		require.Contains(t, packs, *randomPack)
	})
	t.Run("nil pack", func(t *testing.T) {
		t.Parallel()
		err := repo.Create(context.Background(), nil)
		require.ErrorIs(t, err, errors.BadRequest)
	})
}

func TestPortStore_DeletePack(t *testing.T) {
	t.Parallel()
	repo := NewRepository()

	t.Run("delete pack", func(t *testing.T) {
		t.Parallel()

		randomPack := newRandomDomainPack(t)

		err := repo.Create(context.Background(), randomPack)
		require.NoError(t, err)

		packs, err := repo.List(context.Background())
		require.NoError(t, err)
		require.Contains(t, packs, *randomPack)
		err = repo.Delete(context.Background(), randomPack.Size)
		require.NoError(t, err)
		packs, err = repo.List(context.Background())
		require.NoError(t, err)
		require.NotContains(t, packs, *randomPack)
	})
	t.Run("delete nil pack", func(t *testing.T) {
		t.Parallel()
		err := repo.Delete(context.Background(), 0)
		require.ErrorIs(t, err, errors.BadRequest)
	})
	t.Run("delete not existing pack", func(t *testing.T) {
		t.Parallel()
		err := repo.Delete(context.Background(), 1)
		require.ErrorIs(t, err, errors.BadRequest)
	})
}

func newRandomDomainPack(t *testing.T) *domain.Pack {
	t.Helper()
	random := rand.Intn(100)

	newRandom := &domain.Pack{Size: random}

	return newRandom
}
