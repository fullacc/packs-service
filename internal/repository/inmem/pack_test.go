package inmem

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"testing"
	"time"
)

func Test_PackStoreToDomain(t *testing.T) {
	type args struct {
		p *Pack
	}
	tests := []struct {
		name    string
		args    args
		want    *domain.Pack
		wantErr bool
	}{
		{
			name: "should return error when store pack is nil",
			args: args{
				p: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "should return domain pack when store pack is not nil",
			args: args{
				p: newTestStorePack(t),
			},
			want:    newTestDomainPack(t),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got, err := packStoreToDomain(tt.args.p)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

const testSize = 100

func newTestStorePack(t *testing.T) *Pack {
	t.Helper()
	return &Pack{
		Size:      testSize,
		CreatedAt: time.Now(),
	}
}

func newTestDomainPack(t *testing.T) *domain.Pack {
	t.Helper()
	pack := &domain.Pack{Size: testSize}
	return pack
}
