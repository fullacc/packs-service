package inmem

import (
	"errors"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"time"
)

type Pack struct {
	Size int

	CreatedAt time.Time
}

func (p *Pack) Copy() *Pack {
	if p == nil {
		return nil
	}
	return &Pack{
		Size:      p.Size,
		CreatedAt: p.CreatedAt,
	}
}

func packStoreToDomain(p *Pack) (*domain.Pack, error) {
	if p == nil {
		return nil, errors.New("store pack is nil")
	}
	return &domain.Pack{
		p.Size,
	}, nil
}

func packDomainToStore(p *domain.Pack) *Pack {
	return &Pack{
		Size: p.Size,
	}
}
