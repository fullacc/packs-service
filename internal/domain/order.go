package domain

type Order struct {
	Size  int `json:"size"`
	Count int `json:"count"`
}
