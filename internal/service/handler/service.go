package handler

import (
	"context"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"gitlab.com/fullacc/packs-service/internal/interfaces/repositories"
	services2 "gitlab.com/fullacc/packs-service/internal/interfaces/services"
	"sort"
)

type service struct {
	projectRepo repositories.PackRepository
}

func NewService(
	projectRepo repositories.PackRepository,
) services2.HandlerService {
	return &service{
		projectRepo: projectRepo,
	}
}

// Calculate method finds the optimal number of packs to fulfill the order of given size
func (s *service) Calculate(ctx context.Context, size int) ([]domain.Order, error) {
	packs, err := s.projectRepo.List(ctx)
	if err != nil {
		return nil, err
	}
	sort.SliceStable(packs, func(i, j int) bool {
		return packs[i].Size > packs[j].Size
	})
	var ans []domain.Order
	n := len(packs)
	for i, v := range packs {
		counter := 0
		if size > 0 {
			div, rem := size/v.Size, size%v.Size
			if div > 0 {
				counter += div
				size = rem
			}
			if i < n-1 {
				if size-packs[i+1].Size > 0 {
					counter++
					size -= v.Size
				}
			} else {
				counter++
				size -= v.Size
			}
		} else {
			break
		}
		ans = append(ans, domain.Order{
			Size:  v.Size,
			Count: counter,
		})
	}
	return ans, nil
}
