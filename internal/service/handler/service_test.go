package handler

import (
	"context"
	"github.com/stretchr/testify/require"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"gitlab.com/fullacc/packs-service/internal/interfaces/repositories"
	repo_mocks "gitlab.com/fullacc/packs-service/internal/interfaces/repositories/mocks"
	"testing"
)

func TestHandler_Calculate(t *testing.T) {
	type fields struct {
		packRepo repositories.PackRepository
	}
	type args struct {
		ctx  context.Context
		size int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []domain.Order
		wantErr bool
	}{
		{
			name: "ShouldExecuteAndReturnCorrectResult",
			fields: fields{
				packRepo: func() repositories.PackRepository {
					m := repo_mocks.NewPackRepository(t)

					m.
						On("List",
							context.TODO(),
						).
						Return([]domain.Pack{{
							Size: 250,
						}}, nil)

					return m
				}(),
			},
			args: args{
				ctx:  context.TODO(),
				size: 55,
			},
			want:    []domain.Order{{Size: 250, Count: 1}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := NewService(tt.fields.packRepo)
			want, err := h.Calculate(tt.args.ctx, tt.args.size)
			if (err != nil) != tt.wantErr {
				t.Errorf("Calculate() error = %v, wantErr %v", err, tt.wantErr)
			}
			require.EqualValues(t, tt.want, want)
		})
	}
}
