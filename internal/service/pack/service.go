package pack

import (
	"context"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"gitlab.com/fullacc/packs-service/internal/interfaces/repositories"
	services2 "gitlab.com/fullacc/packs-service/internal/interfaces/services"
)

type service struct {
	projectRepo repositories.PackRepository
}

func NewService(
	projectRepo repositories.PackRepository,
) services2.PackService {
	return &service{
		projectRepo: projectRepo,
	}
}

func (s *service) ListPacks(ctx context.Context) ([]domain.Pack, error) {
	return s.projectRepo.List(ctx)
}

func (s *service) DeletePack(ctx context.Context, size int) error {
	return s.projectRepo.Delete(ctx, size)
}

func (s *service) CreatePack(ctx context.Context, pack *domain.Pack) error {
	return s.projectRepo.Create(ctx, pack)
}
