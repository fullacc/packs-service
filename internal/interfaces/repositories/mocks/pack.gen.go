// Code generated by mockery v2.31.4. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	domain "gitlab.com/fullacc/packs-service/internal/domain"
)

// PackRepository is an autogenerated mock type for the PackRepository type
type PackRepository struct {
	mock.Mock
}

// Count provides a mock function with given fields: ctx
func (_m *PackRepository) Count(ctx context.Context) (int, error) {
	ret := _m.Called(ctx)

	var r0 int
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context) (int, error)); ok {
		return rf(ctx)
	}
	if rf, ok := ret.Get(0).(func(context.Context) int); ok {
		r0 = rf(ctx)
	} else {
		r0 = ret.Get(0).(int)
	}

	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Create provides a mock function with given fields: ctx, p
func (_m *PackRepository) Create(ctx context.Context, p *domain.Pack) error {
	ret := _m.Called(ctx, p)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *domain.Pack) error); ok {
		r0 = rf(ctx, p)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Delete provides a mock function with given fields: ctx, size
func (_m *PackRepository) Delete(ctx context.Context, size int) error {
	ret := _m.Called(ctx, size)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, int) error); ok {
		r0 = rf(ctx, size)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// List provides a mock function with given fields: ctx
func (_m *PackRepository) List(ctx context.Context) ([]domain.Pack, error) {
	ret := _m.Called(ctx)

	var r0 []domain.Pack
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context) ([]domain.Pack, error)); ok {
		return rf(ctx)
	}
	if rf, ok := ret.Get(0).(func(context.Context) []domain.Pack); ok {
		r0 = rf(ctx)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]domain.Pack)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewPackRepository creates a new instance of PackRepository. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewPackRepository(t interface {
	mock.TestingT
	Cleanup(func())
}) *PackRepository {
	mock := &PackRepository{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
