//go:generate mockery --name=PackRepository --filename=pack.gen.go

package repositories

import (
	"context"
	"gitlab.com/fullacc/packs-service/internal/domain"
)

type PackRepository interface {
	Create(ctx context.Context, p *domain.Pack) error
	Count(ctx context.Context) (int, error)
	List(ctx context.Context) ([]domain.Pack, error)
	Delete(ctx context.Context, size int) error
}
