//go:generate mockery --name=PackService --filename=pack.gen.go

package services

import (
	"context"
	"gitlab.com/fullacc/packs-service/internal/domain"
)

type PackService interface {
	ListPacks(ctx context.Context) ([]domain.Pack, error)
	CreatePack(ctx context.Context, pack *domain.Pack) error
	DeletePack(ctx context.Context, size int) error
}
