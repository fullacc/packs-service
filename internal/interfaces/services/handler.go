//go:generate mockery --name=HandlerService --filename=handler.gen.go

package services

import (
	"context"
	"gitlab.com/fullacc/packs-service/internal/domain"
)

type HandlerService interface {
	Calculate(ctx context.Context, size int) ([]domain.Order, error)
}
