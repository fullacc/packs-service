package http

import (
	"gitlab.com/fullacc/packs-service/pkg/errors"
	"gitlab.com/fullacc/packs-service/pkg/packs-service"
	"gitlab.com/fullacc/packs-service/pkg/server"
	"net/http"
)

func (s *Server) ListPacks(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()
	response := &packs.V1PackListResponseBody{}

	domainPacks, err := s.packService.ListPacks(ctx)
	if err != nil {
		response.Errors = []string{errors.InternalError.Error()}
		server.RespondOK(response, w, r)
	}
	var resp []packs.Pack
	for _, v := range domainPacks {
		resp = append(resp, packs.Pack{Size: v.Size})
	}
	response.Data = resp
	response.Status = true
	server.RespondOK(response, w, r)
}
