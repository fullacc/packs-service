package http

import (
	"github.com/gorilla/mux"
	"gitlab.com/fullacc/packs-service/docs"
	"gitlab.com/fullacc/packs-service/internal/config"
	"gitlab.com/fullacc/packs-service/internal/interfaces/services"
	"gitlab.com/fullacc/packs-service/pkg/server"
	"log"
	"net/http"
	_ "net/http/pprof"
)

type Server struct {
	cfg         *config.Config
	handler     services.HandlerService
	packService services.PackService
	server      *http.Server
}

func NewServer(
	cfg *config.Config,
	handler services.HandlerService,
	packService services.PackService,
) *Server {
	router := mux.NewRouter()
	serv := &http.Server{
		Handler: router,
	}
	server := &Server{
		cfg:         cfg,
		handler:     handler,
		packService: packService,
		server:      serv,
	}
	router.HandleFunc("/health", Health).Methods("GET")

	router.HandleFunc("/calculate", server.Calculate).Methods("POST")

	router.HandleFunc("/admin/pack", server.CreatePack).Methods("POST")
	router.HandleFunc("/admin/packs", server.ListPacks).Methods("GET")
	router.HandleFunc("/admin/{size}", server.DeletePack).Methods("DELETE")

	admin := router.PathPrefix("/admin").Subrouter()

	uiMiddleware := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.URL.Path = "/openapi_ui" + r.URL.Path[len("/admin/_/docs"):]
			log.Println(r.URL.Path)
			next.ServeHTTP(w, r)
		})
	}

	openapiMiddleware := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.URL.Path = r.URL.Path[len("/admin/_/docs"):]
			next.ServeHTTP(w, r)
		})
	}
	admin.Path("/_/docs/openapi.yaml").Handler(openapiMiddleware(http.FileServer(http.FS(docs.OpenAPISpec)))).Methods("GET")
	admin.PathPrefix("/_/docs/").Handler(uiMiddleware(http.FileServer(http.FS(docs.UI)))).Methods("GET")

	return server
}

func (s *Server) Start(address string) error {
	s.server.Addr = address
	return s.server.ListenAndServe()
}

func (s *Server) Close() error {
	return s.server.Close()
}

func Health(w http.ResponseWriter, r *http.Request) {
	server.RespondOK(BaseHTTPResponse{Status: true}, w, r)
}

type BaseHTTPResponse struct {
	Data   interface{} `json:"data"`
	Errors []string    `json:"errors"`
	Status bool        `json:"status"`
}
