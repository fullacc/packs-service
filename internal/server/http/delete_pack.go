package http

import (
	"github.com/gorilla/mux"
	"gitlab.com/fullacc/packs-service/pkg/errors"
	"gitlab.com/fullacc/packs-service/pkg/packs-service"
	"gitlab.com/fullacc/packs-service/pkg/server"
	"net/http"
	"strconv"
)

func (s *Server) DeletePack(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()
	response := &packs.V1AnyResponseBody{}
	vars := mux.Vars(r)
	size, ok := vars["size"]
	if !ok {
		response.Errors = []string{errors.BadRequest.Error()}
		server.RespondOK(response, w, r)
	}

	packSize, err := strconv.Atoi(size)
	if err != nil {
		response.Errors = []string{errors.InternalError.Error()}
		server.RespondOK(response, w, r)
	}
	err = s.packService.DeletePack(ctx, packSize)
	if err != nil {
		response.Errors = []string{errors.InternalError.Error()}
		server.RespondOK(response, w, r)
	}

	response.Status = true
	server.RespondOK(response, w, r)
}
