package http

import (
	"encoding/json"
	"gitlab.com/fullacc/packs-service/pkg/errors"
	"gitlab.com/fullacc/packs-service/pkg/packs-service"
	"gitlab.com/fullacc/packs-service/pkg/server"
	"net/http"
)

func (s *Server) Calculate(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()

	response := packs.V1CalculatePostResponseBody{}

	var request packs.V1CalculatePostRequestBody
	if err = json.NewDecoder(r.Body).Decode(&request); err != nil {
		response.Errors = []string{errors.BadRequest.Error()}
		server.RespondOK(response, w, r)
	}

	orders, err := s.handler.Calculate(ctx, request.Size)
	if err != nil {
		response.Errors = []string{err.Error()}
		server.RespondOK(response, w, r)
	}
	resp := packs.Order{}
	response.Status = true
	for _, v := range orders {

		resp = append(resp, struct {
			Count int `json:"count"`
			Size  int `json:"size"`
		}{Count: v.Count, Size: v.Size})
	}
	response.Data = resp

	server.RespondOK(response, w, r)
}
