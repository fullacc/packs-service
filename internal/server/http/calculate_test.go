package http

import (
	"bytes"
	"context"
	"encoding/json"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"gitlab.com/fullacc/packs-service/internal/interfaces/services"
	service_mocks "gitlab.com/fullacc/packs-service/internal/interfaces/services/mocks"
	"gitlab.com/fullacc/packs-service/pkg/packs-service"
	"net/http/httptest"
	"reflect"

	"net/http"
	"testing"
)

func TestServer_Calculate(t *testing.T) {

	testCases := []struct {
		name         string
		requestBody  []byte
		expectedCode int
		expectedData packs.V1CalculatePostResponseBody
		handler      services.HandlerService
	}{
		{
			name:         "Valid Input",
			requestBody:  []byte(`{"size": 100}`),
			expectedCode: http.StatusOK,
			expectedData: packs.V1CalculatePostResponseBody{
				Data: []struct {
					Count int `json:"count"`
					Size  int `json:"size"`
				}{{Count: 1, Size: 250}},
				Errors: nil,
				Status: true,
			},
			handler: func() services.HandlerService {
				m := service_mocks.NewHandlerService(t)

				m.On("Calculate",
					context.TODO(),
					100,
				).
					Return([]domain.Order{{
						Size:  250,
						Count: 1,
					}}, nil)

				return m
			}(),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("POST", "/calculate", bytes.NewBuffer(tc.requestBody))
			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()

			server := &Server{
				handler: tc.handler,
			}

			server.Calculate(rr, req)

			if rr.Code != tc.expectedCode {
				t.Errorf("Expected status code %d, but got %d", tc.expectedCode, rr.Code)
			}

			var response packs.V1CalculatePostResponseBody
			if err := json.NewDecoder(rr.Body).Decode(&response); err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(response, tc.expectedData) {
				t.Errorf("Expected response data %+v, but got %+v", tc.expectedData, response)
			}
		})
	}
}
