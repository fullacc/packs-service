package http

import (
	"encoding/json"
	"gitlab.com/fullacc/packs-service/internal/domain"
	"gitlab.com/fullacc/packs-service/pkg/errors"
	"gitlab.com/fullacc/packs-service/pkg/packs-service"
	"gitlab.com/fullacc/packs-service/pkg/server"
	"net/http"
)

func (s *Server) CreatePack(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()

	response := &packs.V1PackCreateResponseBody{}

	var request packs.V1PackCreateRequestBody
	if err = json.NewDecoder(r.Body).Decode(&request); err != nil {
		response.Errors = []string{errors.BadRequest.Error()}
		server.RespondOK(response, w, r)
	}
	err = s.packService.CreatePack(ctx, &domain.Pack{Size: request.Size})
	if err != nil {
		response.Errors = []string{errors.InternalError.Error()}
		server.RespondOK(response, w, r)
	}

	response.Status = true
	server.RespondOK(response, w, r)
}
