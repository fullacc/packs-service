# Packs Service

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

Packs Service is a Go-based microservice for Calculating number of packs to fulfill order.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
    - [Installation](#installation)
    - [Configuration](#configuration)
- [Usage](#usage)
    - [Build](#build)
    - [Code Generation](#code-generation)
    - [Run Tests](#run-tests)
    - [Generate Mocks](#generate-mocks)
- [Docker](#docker)
- [License](#license)

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Go 1.20 or later installed
- Docker Compose

## Getting Started

To get the project up and running, follow these steps:

### Installation

```bash
# Clone the repository
git clone https://gitlab.com/fullacc/pack-service.git

# Change to the project directory
cd pack-service

make dc
```

### Configuration

No specific configuration is required at this time.
### Usage

Here are some common tasks and how to perform them:
### Build

To build the project, run:

``` bash
make build
```
### Code Generation

To generate code from the OpenAPI specification, run:

``` bash
make codegen-oapi
```
### Run Tests

To run tests, use the following command:

``` bash
make run-tests
```
### Generate Mocks

To generate mocks, run:

```bash
make generate-mocks
```
### Docker

You can also run the Packs Service in a Docker container:
### Build Docker Image

```bash
docker build -t packs-service .
```
### Run Docker Container

```bash
docker run -d -p 8100:8100 packs-service
```
### License

This project is licensed under the MIT License - see the LICENSE file for details.
